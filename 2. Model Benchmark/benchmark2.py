import pandas as pd
from format_submission import format_submission2
from catboost import Pool, CatBoostClassifier, CatBoostRegressor
from utility import *

#### 1. Importing and Preparing Data ####


print("Step 1/4: Importing and Preparing Data")

train = pd.read_csv("../0. Input Data/train.csv", sep=",")
train['date'] = pd.to_datetime(train['date'])
train['Postcode'] = train['Postcode'].astype(str)

n = len(train)
n_train = 0.8*n
train_station = train#.loc[0:n_train,]
# print(train_station)
valid_station = train.loc[n_train:n,]
# print(valid_station)

test_station = pd.read_csv("../0. Input Data/test.csv", sep=",")
test_station['date'] = pd.to_datetime(test_station['date'])
test_station['Postcode'] = test_station['Postcode'].astype(str)

train_area = train_station.groupby(['date', 'area']).agg({'Available': 'sum',
                                                         'Charging': 'sum',
                                                          'Passive': 'sum',
                                                          'Other': 'sum',
                                                          'tod': 'max',
                                                          'dow': 'max',
                                                          'Latitude': 'mean',
                                                          'Longitude': 'mean',
                                                          'trend': 'max'}).reset_index()
valid_area = valid_station.groupby(['date', 'area']).agg({'Available': 'sum',
                                                         'Charging': 'sum',
                                                          'Passive': 'sum',
                                                          'Other': 'sum',
                                                          'tod': 'max',
                                                          'dow': 'max',
                                                          'Latitude': 'mean',
                                                          'Longitude': 'mean',
                                                          'trend': 'max'}).reset_index()

test_area = test_station.groupby(['date', 'area']).agg({
    'tod': 'max',
    'dow': 'max',
    'Latitude': 'mean',
    'Longitude': 'mean',
    'trend': 'max'}).reset_index()


train_global = train_station.groupby('date').agg({'Available': 'sum',
                                                  'Charging': 'sum',
                                                  'Passive': 'sum',
                                                  'Other': 'sum',
                                                  'tod': 'max',
                                                  'dow': 'max',
                                                  'trend': 'max'}).reset_index()
valid_global = valid_station.groupby('date').agg({'Available': 'sum',
                                                  'Charging': 'sum',
                                                  'Passive': 'sum',
                                                  'Other': 'sum',
                                                  'tod': 'max',
                                                  'dow': 'max',
                                                  'trend': 'max'}).reset_index()
test_global = test_station.groupby('date').agg({
    'tod': 'max',
    'dow': 'max',
    'trend': 'max'}).reset_index()

#### 2. Target Analysis ####

print("Step 2/4: Target Analysis")

station_features = ['Station', 'tod', 'dow', 'area'] + \
    ['trend', 'Latitude', 'Longitude']  # temporal and spatial inputs
area_features = ['area', 'tod', 'dow'] + ['trend',
                                          'Latitude', 'Longitude']  # temporal and spatial inputs
global_features = ['tod', 'dow'] + ['trend']  # temporal input
targets = ["Available","Charging","Passive","Other"] # targets

#### 3. Modelling ####

print("Step 3/4: Modelling")

### a. Station ###
print("=== Station ===")
station_models = catboost_training(
                                  train = train_station,
                                  test = valid_station,
                                  features = station_features,
                                  cat_features = [0,1,2,3],
                                  targets = targets,
                                  learning_rate = 0.1,
                                  classif = False)

s_pred = catboost_prediction(station_models,
                             test_station,
                             features = station_features,
                             targets=targets,
                             level_col='Station')

### b. Area ###
print("=== Area ===")
area_models = catboost_training(
                                train = train_area,
                                test = valid_area,
                                features = area_features,
                                cat_features = [0,1,2],
                                targets = targets,
                                learning_rate = 0.1
)
a_pred = catboost_prediction(area_models,
                             test_area,
                             features = area_features,
                             targets=targets,
                             level_col='area')

### c. Global ###
print("=== Global ===")
global_models = catboost_training(
                                  train = train_global,
                                  test = valid_global,
                                  features = global_features,
                                  cat_features = [0,1],
                                  targets = targets,
                                  learning_rate=0.1
)
g_pred = catboost_prediction(global_models,
                             test_global,
                             features = global_features,
                             targets=targets,
                             level_col=None)

#### 4. Output prediction ####
print(s_pred)
print(a_pred)
print(g_pred)
print("Step 4/4: Output prediction")
format_submission2(s_pred, a_pred, g_pred, test_station)

print("Completed! Your submission sample is named sample_result_submission.zip, feel free to upload it directly to the leaderboard!")
