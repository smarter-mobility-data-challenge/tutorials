import optuna
import torch
import pandas as pd
import torch.nn.functional as F
from torch.utils.data import DataLoader, TensorDataset
import numpy as np
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
import numpy as np
from my_metric import sae, overall_metric
import copy

##### Utility functions

def prep_data_train(df_train,num_features,cat_features,labels,device,batch_size):
    """
    df_train : DataFrame, train data
    device : CPU or GPU
    batch_size : int, batch size of the data loader
    """
    # One hot encoding
    ohe = OneHotEncoder(drop='first', sparse_output=False, handle_unknown='ignore')
    encoded = ohe.fit_transform(df_train[cat_features])
    encoded_df = pd.DataFrame(encoded, columns=ohe.get_feature_names_out(cat_features))
    
    # Extract features and labels
    features = pd.concat([df_train[num_features].reset_index(drop=True), encoded_df.reset_index(drop=True)], axis=1).astype(float).values
    labels = df_train[labels].astype(float).values

    # Apply Min-Max scaling
    scaler = MinMaxScaler()
    features = scaler.fit_transform(features)

    # Convert to PyTorch tensors and move to device
    X = torch.tensor(features, dtype=torch.float).to(device)
    Y = torch.tensor(labels, dtype=torch.float).to(device)
    
    # Create a TensorDataset and DataLoader
    dataset = TensorDataset(X, Y)
    data_loader = DataLoader(dataset, batch_size=batch_size, shuffle=True)
    
    return data_loader, scaler, ohe, X, Y

def prep_data_test(df_test,num_features,cat_features,device,scaler,ohe):
    """
    df_test : DataFrame, test data
    device : CPU or GPU
    batch_size : int, batch size of the data loader
    """
    # One hot encoding
    encoded = ohe.transform(df_test[cat_features])
    encoded_df = pd.DataFrame(encoded, columns=ohe.get_feature_names_out(cat_features))
    
    # Extract features and labels
    features = pd.concat([df_test[num_features].reset_index(drop=True), encoded_df.reset_index(drop=True)], axis=1).astype(float).values

    # Apply Min-Max scaling
    features = scaler.transform(features)

    # Convert to PyTorch tensors and move to device
    X = torch.tensor(features, dtype=torch.float).to(device)
    
    return X, scaler, ohe

def predict(data,X,model):
    model.eval()
    with torch.no_grad():
        outputs = model(X)
    predictions = outputs.cpu().numpy() 
    predictions[predictions < 0] = 0
    predictions = predictions.round()
    return(pd.concat([data[['date','Station','Postcode','area']],pd.DataFrame(data = predictions, columns = ['Available', 'Charging', 'Passive', 'Other'])],axis=1))

def calculate_score(train_actual,train_pred,final):

    targets = ['Available', 'Charging', 'Passive', 'Other']
    ############## train
    
    #reference train data
    actual_train = {}
    actual_train['station'] = train_actual
    actual_train['station'][targets] = round(train_actual[targets])
    ### Deriving area and global predictions from station forecasts
    actual_train['area'] = train_actual[['date','area'] + targets].groupby(['date','area'], as_index=False).sum()
    actual_train['global'] = train_actual[['date'] + targets].groupby(['date'], as_index=False).sum()

    
    #predicted test data
    predicted_train = {}
    predicted_train['station'] = train_pred

    ### Rounding predictions
    predicted_train['station'][targets] = round(train_pred[targets])

    ### Deriving area and global predictions from station forecasts
    predicted_train['area'] = train_pred[['date','area'] + targets].groupby(['date','area'], as_index=False).sum()
    predicted_train['global'] = train_pred[['date'] + targets].groupby(['date'], as_index=False).sum()

    # score
    score = round(overall_metric(actual_train, predicted_train, pd.DataFrame(train_actual['date'].unique(),columns=['date'])),3)

    ################ test
    
    #dates
    public_dates = pd.read_csv("../6. Bundle/ref/public_dates.csv")
    private_dates = pd.read_csv("../6. Bundle/ref/private_dates.csv")
    
    #reference test data
    actual = {}
    actual['station'] = pd.read_csv("../6. Bundle/ref/reference_station.csv")
    actual['area'] = pd.read_csv("../6. Bundle/ref/reference_area.csv")
    actual['global'] = pd.read_csv("../6. Bundle/ref/reference_global.csv")

    #predicted test data
    predicted = {}
    predicted['station'] = final

    ### Rounding predictions
    predicted['station'][targets] = round(predicted['station'][targets])

    ### Deriving area and global predictions from station forecasts
    predicted['area'] = predicted['station'][['date','area'] + targets].groupby(['date','area'], as_index=False).sum()
    predicted['global'] = predicted['station'][['date'] + targets].groupby(['date'], as_index=False).sum()

    # copies
    actual_public = copy.deepcopy(actual)
    actual_private = copy.deepcopy(actual)
    predicted_public = copy.deepcopy(predicted)
    predicted_private = copy.deepcopy(predicted)
    
    # score
    score_public = round(overall_metric(actual_public, predicted_public, public_dates),3)
    score_private = round(overall_metric(actual_private, predicted_private, private_dates),3)

    print('train_score:'+str(score))
    print('public_score:'+str(score_public))
    print('private_score:'+str(score_private))

    with open('nn/fcnn/submission/output.txt', 'w') as file:
        # Write each string to the file, each on a new line
        file.write('train_score:'+str(score) + '\n')
        file.write('public_score:'+str(score_public) + '\n')
        file.write('private_score:'+str(score_private) + '\n')

##### Model class

class FCNNModel_reg(torch.nn.Module):
    def __init__(self, num_features, hidden_dim, dropout):
        super(FCNNModel_reg, self).__init__()
        self.fc1 = torch.nn.Linear(num_features, hidden_dim)
        self.dropout = torch.nn.Dropout(dropout)
        self.fc3 = torch.nn.Linear(hidden_dim, 4)
    
    def forward(self, x):
        x = F.relu(self.fc1(x))  
        x = self.dropout(x)
        x = self.fc3(x)
        return x
    
#### END