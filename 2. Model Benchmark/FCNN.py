from fcnn_utility import *
from format_submission import *
import warnings
import pickle

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

df = pd.read_csv('../0. Input Data/train.csv')
num_features = ['Latitude','Longitude','trend']
cat_features = ['Station','tod', 'dow', 'Postcode', 'area']
labels = ['Available', 'Charging', 'Passive', 'Other']

def objective_fcnn(trial):
    # Suggest hyperparameters for the model
    hidden_dim = trial.suggest_int('hidden_dim', 10, 200)
    lr = trial.suggest_float('lr', 1e-4, 1e-2, log=True)
    epochs = trial.suggest_int('epochs', 5, 50)
    dropout = trial.suggest_float('dropout', 0.0, 0.5)
    batch_size = trial.suggest_int('batch_size', 128, 1024)

    # Create data_loader
    data_loader,scaler,ohe,_,_ = prep_data_train(df,num_features,cat_features,labels,device,batch_size)
    
    # Create the FCNN model
    model = FCNNModel_reg(num_features=data_loader.dataset.tensors[0].shape[1], hidden_dim=hidden_dim, dropout=dropout).to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)

    best_loss = float('inf')  # Initialize best loss to infinity
    best_model_state = None

    for epoch in range(epochs):
        model.train()
        for batch in data_loader:
            optimizer.zero_grad()
            
            # Move features and labels to the device
            inputs, targets = batch
            inputs, targets = inputs.to(device), targets.to(device)
            
            # Pass features through the model
            outputs = model(inputs)
            
            # Compute loss
            loss = F.mse_loss(outputs, targets)
            loss.backward()
            optimizer.step()
        
        model.eval()
        val_loss = 0
        with torch.no_grad():
            for batch in data_loader:
                inputs, targets = batch
                inputs, targets = inputs.to(device), targets.to(device)
                outputs = model(inputs)
                val_loss += F.mse_loss(outputs, targets).item()
            val_loss /= len(data_loader)
        
        # Save the model if the validation loss is the best so far
        if val_loss < best_loss:
            best_loss = val_loss
            best_model_state = model.state_dict()

    # Save the best model state from this trial
    if best_model_state is not None:
        trial_model_filename = f'nn/fcnn/trial_{trial.number}_best_model_fcnn.pth'
        torch.save(best_model_state, trial_model_filename)

    return best_loss


study = optuna.create_study(direction='minimize')
study.optimize(objective_fcnn, n_trials=20)

print(f"Best hyperparameters: {study.best_params}")
print(f"Best loss: {study.best_value}")

# Save the study to a .pkl file
with open("nn/fcnn/study.pkl", "wb") as f:
    pickle.dump(study, f)

data_loader,scaler,ohe,X,_ = prep_data_train(df,num_features,cat_features,labels,device,batch_size=study.best_params['batch_size'])
model_fcnn = FCNNModel_reg(num_features=X.shape[1], hidden_dim=study.best_params['hidden_dim'], dropout=study.best_params['dropout']).to(device)
print(model_fcnn)

model_fcnn.load_state_dict(torch.load('nn/fcnn/trial_'+str(study.best_trial.number)+'_best_model_fcnn.pth'))
train_actual = pd.read_csv('../0. Input Data/train.csv')
train_pred = predict(train_actual,X,model_fcnn)

test = pd.read_csv('../0. Input Data/test.csv')
X_test,_,_ = prep_data_test(test,num_features,cat_features,device,scaler,ohe)
test_pred = predict(test,X_test,model_fcnn)


warnings.filterwarnings("ignore", category=FutureWarning)
calculate_score(train_actual,train_pred,test_pred)
test_pred = test_pred[['date','area','Station','Available','Charging','Passive','Other']]
test_pred.to_csv('nn/fcnn/submission/station.csv')


### END














