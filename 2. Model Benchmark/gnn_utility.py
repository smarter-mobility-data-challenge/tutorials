import optuna
import torch
import pandas as pd
import torch.nn.functional as F
from torch_geometric.nn import GATConv
from torch_geometric.data import Data
from torch_geometric.loader import DataLoader
import numpy as np
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
from scipy.spatial.distance import cdist
import numpy as np
import folium
import networkx as nx
import numpy as np
import pandas as pd
from folium import plugins
from my_metric import sae, overall_metric
import copy

##### Utility functions

def adj_map(unique_stations,adjacency_matrix):
    # Sample data
    latitude = unique_stations.Latitude
    longitude = unique_stations.Longitude
    
    # Sample adjacency matrix
    adjacency_matrix = adjacency_matrix
    
    # Create a graph from the adjacency matrix
    G = nx.from_numpy_array(adjacency_matrix)
    
    # Create a Folium map centered at the average latitude and longitude
    m = folium.Map(location=[np.mean(latitude), np.mean(longitude)], zoom_start=13)
    
    # Add nodes to the map
    for i, (lat, lon) in enumerate(zip(latitude, longitude)):
        folium.Marker([lat, lon], tooltip=f'Node {i}').add_to(m)
    
    # Add edges to the map
    for (i, j) in G.edges():
        folium.PolyLine(
            locations=[[latitude[i], longitude[i]], [latitude[j], longitude[j]]],
            color='blue',
            weight=2.5,
            opacity=0.8
        ).add_to(m)
    
    # Optionally add plugins for interactivity
    plugins.HeatMap([[lat, lon] for lat, lon in zip(latitude, longitude)]).add_to(m)
    
    # Save to an HTML file and display
    m.save('graph_map.html')
    print("Map has been saved to 'graph_map.html'. Open this file in your browser to view the map.")



    # # One hot encoding
    # ohe = OneHotEncoder(drop='first', sparse_output=False, handle_unknown='ignore')
    # encoded = ohe.fit_transform(df_train[cat_features])
    # encoded_df = pd.DataFrame(encoded, columns=ohe.get_feature_names_out(cat_features))
    
    # # Extract features and labels
    # features = pd.concat([df_train[num_features].reset_index(drop=True), encoded_df.reset_index(drop=True)], axis=1).astype(float).values
    # labels = df_train[labels].astype(float).values

    # # Apply Min-Max scaling
    # scaler = MinMaxScaler()
    # features = scaler.fit_transform(features)

    # # Convert to PyTorch tensors and move to device
    # X = torch.tensor(features, dtype=torch.float).to(device)
    # Y = torch.tensor(labels, dtype=torch.float).to(device)
    
    # # Create a TensorDataset and DataLoader
    # dataset = TensorDataset(X, Y)
    # data_loader = DataLoader(dataset, batch_size=batch_size, shuffle=True)
    
    # return data_loader, scaler, ohe, X, Y


def prep_data_train(df_train,device,batch_size,threshold=0.01):
    """
    df_train : DataFrame, train data
    device : CPU or GPU
    batch_size : int, batch size of the data loader
    threshold : float, define a threshold for connecting nodes in the adjacency matrix => default = 0.01
    """
    
    # One-hot encode categorical features
    categorical_features = ['tod', 'dow', 'Postcode', 'area']
    labels = ['Available', 'Charging', 'Passive', 'Other']
    df_train = pd.get_dummies(df_train, columns=categorical_features, drop_first=True)
    
    # Extract node features and labels
    node_features = df_train.drop(columns=labels + ['date', 'Station', 'Latitude', 'Longitude', 'trend']).astype(float).values
    unique_stations = df_train[['Station', 'Latitude', 'Longitude']].groupby('Station').agg('mean')

    coords = unique_stations[['Latitude', 'Longitude']].values
    distances = cdist(coords, coords, 'euclidean')
    adjacency_matrix = (distances < threshold).astype(int)
    np.fill_diagonal(adjacency_matrix, 0)

    # Convert to PyTorch tensors and move to GPU
    edge_index = torch.tensor(np.array(np.nonzero(adjacency_matrix)), dtype=torch.long).to(device)
    x = torch.tensor(node_features, dtype=torch.float).to(device)
    y = torch.tensor(df_train[labels].values, dtype=torch.float).to(device)
    
    # Create PyTorch Geometric Data object
    data_list = []
    for i in range(0, len(x), batch_size):
        batch_x = x[i:i + batch_size]
        batch_y = y[i:i + batch_size]
        batch_edge_index = edge_index[:, (edge_index[0] < i + batch_size) & (edge_index[1] < i + batch_size)]

        data_list.append(Data(x=batch_x, edge_index=batch_edge_index, y=batch_y))
    
    # Create DataLoader
    data_loader = DataLoader(data_list, batch_size=batch_size)

    return unique_stations, adjacency_matrix, edge_index, x, y, data_list, data_loader

##### Model class

class GATModel(torch.nn.Module):
    def __init__(self, num_features, hidden_dim, num_heads):
        super(GATModel, self).__init__()
        self.gat1 = GATConv(num_features, hidden_dim, heads=num_heads)
        self.fc = torch.nn.Linear(hidden_dim * num_heads, 4)
    
    def forward(self, x, edge_index):
        x = self.gat1(x, edge_index)
        x = F.elu(x)
        x = self.fc(x)
        return x

# Define your GAT model
# class GAT(torch.nn.Module):
#     def __init__(self, in_channels, hidden_channels, out_channels, num_heads):
#         super(GAT, self).__init__()
#         self.conv1 = GATConv(in_channels, hidden_channels, heads=num_heads, dropout=0.6)
#         self.conv2 = GATConv(hidden_channels * num_heads, out_channels, heads=1, concat=False, dropout=0.6)
    
#     def forward(self, x, edge_index):
#         x = F.elu(self.conv1(x, edge_index))
#         x = self.conv2(x, edge_index)
#         return x
    
#### END