# import numpy as np
import pandas as pd


def sae(y_true, y_pred):
    """Sum of Absolute errors"""
    return sum(abs(y_true - y_pred))


def overall_metric(actual_dict, predicted_dict, filter_dates):
    actual_dict["global"]["date"] = pd.to_datetime(actual_dict["global"]["date"])
    actual_dict["area"]["date"] = pd.to_datetime(actual_dict["area"]["date"])
    actual_dict["station"]["date"] = pd.to_datetime(actual_dict["station"]["date"])

    predicted_dict["global"]["date"] = pd.to_datetime(predicted_dict["global"]["date"])
    predicted_dict["area"]["date"] = pd.to_datetime(predicted_dict["area"]["date"])
    predicted_dict["station"]["date"] = pd.to_datetime(predicted_dict["station"]["date"])

    actual_dict["global"] = (
        actual_dict["global"][actual_dict["global"]["date"].isin(filter_dates["date"])]
        .sort_values(by="date", ascending=True)
        .reset_index(drop=True)
    )
    actual_dict["area"] = (
        actual_dict["area"][actual_dict["area"]["date"].isin(filter_dates["date"])]
        .sort_values(by=["date", "area"], ascending=True)
        .reset_index(drop=True)
    )
    actual_dict["station"] = (
        actual_dict["station"][actual_dict["station"]["date"].isin(filter_dates["date"])]
        .sort_values(by=["date", "Station"], ascending=True)
        .reset_index(drop=True)
    )

    predicted_dict["global"] = (
        predicted_dict["global"][predicted_dict["global"]["date"].isin(filter_dates["date"])]
        .sort_values(by="date", ascending=True)
        .reset_index(drop=True)
    )
    predicted_dict["area"] = (
        predicted_dict["area"][predicted_dict["area"]["date"].isin(filter_dates["date"])]
        .sort_values(by=["date", "area"], ascending=True)
        .reset_index(drop=True)
    )
    predicted_dict["station"] = (
        predicted_dict["station"][predicted_dict["station"]["date"].isin(filter_dates["date"])]
        .sort_values(by=["date", "Station"], ascending=True)
        .reset_index(drop=True)
    )

    N = len(filter_dates)

    actual_dict["global"]["geo"] = "global"
    actual_dict["global"]["geo_type"] = "global"
    actual_dict["global"]["pred_actual"] = "actual"
    actual_dict["global"] = pd.melt(
        actual_dict["global"],
        id_vars=["date", "geo", "geo_type", "pred_actual"],
        value_vars=["Available", "Charging", "Passive", "Other"],
        var_name="target",
    )

    predicted_dict["global"]["geo"] = "global"
    predicted_dict["global"]["geo_type"] = "global"
    predicted_dict["global"]["pred_actual"] = "pred"
    predicted_dict["global"] = pd.melt(
        predicted_dict["global"],
        id_vars=["date", "geo", "geo_type", "pred_actual"],
        value_vars=["Available", "Charging", "Passive", "Other"],
        var_name="target",
    )

    actual_dict["area"]["geo_type"] = "area"
    actual_dict["area"]["pred_actual"] = "actual"
    actual_dict["area"].rename(columns={"area": "geo"}, inplace=True)
    actual_dict["area"] = pd.melt(
        actual_dict["area"],
        id_vars=["date", "geo", "geo_type", "pred_actual"],
        value_vars=["Available", "Charging", "Passive", "Other"],
        var_name="target",
    )

    predicted_dict["area"]["geo_type"] = "area"
    predicted_dict["area"]["pred_actual"] = "pred"
    predicted_dict["area"].rename(columns={"area": "geo"}, inplace=True)
    predicted_dict["area"] = pd.melt(
        predicted_dict["area"],
        id_vars=["date", "geo", "geo_type", "pred_actual"],
        value_vars=["Available", "Charging", "Passive", "Other"],
        var_name="target",
    )

    actual_dict["station"]["geo_type"] = "station"
    actual_dict["station"]["pred_actual"] = "actual"
    actual_dict["station"].rename(columns={"Station": "geo"}, inplace=True)
    actual_dict["station"] = pd.melt(
        actual_dict["station"],
        id_vars=["date", "geo", "geo_type", "pred_actual"],
        value_vars=["Available", "Charging", "Passive", "Other"],
        var_name="target",
    )

    predicted_dict["station"]["geo_type"] = "station"
    predicted_dict["station"]["pred_actual"] = "pred"
    predicted_dict["station"].rename(columns={"Station": "geo"}, inplace=True)
    predicted_dict["station"] = pd.melt(
        predicted_dict["station"],
        id_vars=["date", "geo", "geo_type", "pred_actual"],
        value_vars=["Available", "Charging", "Passive", "Other"],
        var_name="target",
    )

    return pd.concat(
        [
            actual_dict["station"],
            predicted_dict["station"],
            actual_dict["area"],
            predicted_dict["area"],
            actual_dict["global"],
            predicted_dict["global"],
        ]
    )
