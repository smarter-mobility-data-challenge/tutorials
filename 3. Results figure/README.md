# Results figure

In this folder, we take the candidates' best submission (*stored in ./candidates_best_submissions/*) and compare them with the ground truth (*stored in ./reference/*).

### First run 1_evaluate_boostrapped_mean_and_std.py
We calculate a team's total score, and the contribution of all hierarchical levels towards that score (*active, passive, charging, other* as well as *global, zone, station*).
We use basic bootstrapping to obtain the variance with which each score is estimated.

### Then run 2_make_figure.py
In this second script, we display the teams' scores on the figure used in the NeurIPS article.
