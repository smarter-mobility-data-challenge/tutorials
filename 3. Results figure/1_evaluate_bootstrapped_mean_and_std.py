import itertools
import os
import random
import sys
import time
from pathlib import Path

import numpy as np
import pandas as pd

from util.metric_calculation import overall_metric

"""
This script reads the candidates' submission files, and applies Moving Block Bootstrapping to obtain the confidence
interval around the candidates' score estimation.
The results are written on disk in the file scores_by_subgroup_mean_and_std.parquet
"""

def mkdir(d):
    if not os.path.exists(d):
        os.makedirs(d)


base_dir = Path(__file__).resolve().parents[0]
ref_dir = base_dir / "reference"
submissions_dir = base_dir / "candidates_best_submissions"

team_dir = {}
# Dictionary structure
# {
#   key  : team name,
#   value: path of directory where the solution is stored
# }
for child in itertools.islice(submissions_dir.iterdir(), 100):
    team_dir[child.name.split(" - ")[0]] = child


# ==============================================================================


def grouped_and_moving_blocks_tsbootstrap(abs_error, query, n_col, N):
    sub_dfs = []
    # Crude implementation of Moving Block Bootstrap, with 96h blocks
    for offset_h in range(0, 96, 3):
        sub_dfs += [
            sub_df
            for group, sub_df in abs_error.reset_index()
            .query(query)
            .set_index("date")
            .groupby([pd.Grouper(freq="4D", offset=f"{offset_h}H")])
        ]

    return [get_single_bootstrap_score(sub_dfs, n_col, N) for _ in range(n_bootstraps)]


def get_single_bootstrap_score(sub_dfs, n_col, N):
    df = random.choice(sub_dfs).copy()
    while len(df) < N * n_col:
        df = pd.concat([df] + random.choices(sub_dfs, k=1))

    df = df.iloc[: N * n_col]
    return df["abs_error"].sum() / N


# =============================== MAIN ========================================

targets = ["Available", "Charging", "Passive", "Other"]
all_scores_and_std = []
n_bootstraps = 400

for team_name, sub_dir in team_dir.items():
    private_dates = pd.read_csv(ref_dir / "private_dates.csv")
    actual = {}
    actual["station"] = pd.read_csv(ref_dir / "reference_station.csv")
    actual["area"] = pd.read_csv(ref_dir / "reference_area.csv")
    actual["global"] = pd.read_csv(ref_dir / "reference_global.csv")

    ### Inspecting submission
    files_list = os.listdir(sub_dir)
    files_number = len(files_list)

    if files_number == 1:
        ### Bottom-up automatic forecast

        ### Loading prediction file
        predicted = {}
        predicted["station"] = pd.read_csv(sub_dir / "station.csv")

        ### Rounding predictions
        predicted["station"][targets] = round(predicted["station"][targets])

        ### Deriving area and global predictions from station forecasts
        predicted["area"] = predicted["station"][["date", "area"] + targets].groupby(["date", "area"], as_index=False).sum()
        predicted["global"] = predicted["station"][["date"] + targets].groupby(["date"], as_index=False).sum()

    elif files_number == 3:
        ### Normal forecast

        ### Loading prediction files
        predicted = {
            "station": pd.read_csv(sub_dir / "station.csv"),
            "area": pd.read_csv(sub_dir / "area.csv"),
            "global": pd.read_csv(sub_dir / "global.csv"),
        }

        ### Rounding predictions
        predicted["station"][targets] = round(predicted["station"][targets])
        predicted["area"][targets] = round(predicted["area"][targets])
        predicted["global"][targets] = round(predicted["global"][targets])

    else:
        print("The zip archive contains", files_number, "files")
        sys.exit("Error: The zip archive needs to contain either one or three csv files")

    if len(actual) != len(predicted):
        sys.exit("Error: The submitted file is not of the same length as the reference file")

    # ===========
    df = overall_metric(actual, predicted, private_dates)

    abs_error = (
        df.set_index(["date", "geo", "geo_type", "pred_actual", "target"])
        .unstack(3)
        .diff(axis=1)["value"]["pred"]
        .abs()
        .rename("abs_error")
    )
    abs_error_pivoted = abs_error.reset_index().pivot(index="date", columns=["geo_type", "geo", "target"], values="abs_error")

    N = len(abs_error_pivoted)

    scores = {}

    scores["overall"] = np.array(grouped_and_moving_blocks_tsbootstrap(abs_error, "index == index or index != index", 384, N))

    scores["global"] = np.array(grouped_and_moving_blocks_tsbootstrap(abs_error, "geo_type == 'global'", 4, N))
    scores["area"] = np.array(grouped_and_moving_blocks_tsbootstrap(abs_error, "geo_type == 'area'", 16, N))
    scores["station"] = np.array(grouped_and_moving_blocks_tsbootstrap(abs_error, "geo_type == 'station'", 364, N))

    scores["available"] = np.array(grouped_and_moving_blocks_tsbootstrap(abs_error, "target == 'Available'", 96, N))
    scores["charging"] = np.array(grouped_and_moving_blocks_tsbootstrap(abs_error, "target == 'Charging'", 96, N))
    scores["passive"] = np.array(grouped_and_moving_blocks_tsbootstrap(abs_error, "target == 'Passive'", 96, N))
    scores["other"] = np.array(grouped_and_moving_blocks_tsbootstrap(abs_error, "target == 'Other'", 96, N))

    for score_subgroup, score_values in scores.items():
        all_scores_and_std.append(
            {"team_name": team_name, "score_subgroup": score_subgroup, "mean_std": "mean", "value": score_values.mean()}
        )
        all_scores_and_std.append(
            {"team_name": team_name, "score_subgroup": score_subgroup, "mean_std": "std", "value": score_values.std()}
        )

    print(f"Boostrapped score calculation is done for {team_name}", time.time())

pd.DataFrame(all_scores_and_std).to_parquet("scores_by_subgroup_mean_and_std.parquet")
print(f"Done. Saved boostrapped scores to scores_by_subgroup_mean_and_std.parquet")
