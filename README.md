<br />
<p align="center">

  <h1 align="center">Smarter Mobility Data Challenge - Tutorials</h1>

  <p align="center">
    Sample codes and dashboards for supporting participants throughout the competition
    <br />
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li><a href="#about-the-project">About The Project</a></li>
	<li><a href="#getting-started">Getting Started</a></li>
    <li><a href="#prerequisites">Prerequisites</a></li>
    <li><a href="#authors">Authors</a></li>
    <li><a href="#license">License</a></li>
  </ol>
</details>

## About The Project

This challenge aims at testing statistical and machine learning forecasting models to forecast the states of a set of charging station in the paris area at different geographical resolution. This repository is for participants to get insights from the organizers on the data provided.

## Getting Started

This repository contains the data and codes produced for the tutorials that will be delivered throughout the challenge:

* [0. Input Data](./0.%20Input%20Data) contains the train.csv file that will be used for visualization the data. It is the same file as the one available on [CodaLab](https://codalab.lisn.upsaclay.fr/competitions/7192#participate-get_starting_kit).
* [1. Data Viz](./1.%20Data%20Viz) contains the R and Python codes as well as Tableau dashboards presented in the tutorial. Feel free to use them as inspiration for your own data visualizations and as a starting point to training your models.
* [2. Model Benchmark](./2.%20Model%20Benchmark) contains the starting-kit available on CodaLab as well as two benchmark models proposed as baselines for the challenge.

## Prerequisites

The typical data visualization libraries and packages of Python and R are used.

It is recommended to create a new Python environment and install the required libraries listed in requirements.txt

```console
foo@bar:~$ python -m venv .venv
foo@bar:~$ ./.venv/Scripts/activate
(.venv) foo@bar:~$ pip install -r requirements.txt
```

## Solutions
Here are the links to the git projects gathering the top three solutions
* **Daniel Hebenstreit & Thomas Wedenig** https://gitlab.com/T3chy1/smarter-mobilty-data-challenge
* **Arthur Sarmini Det Satouf** https://github.com/arthur-75/Smarter-Mobility-Data-Challenge
* **Nathan Doumèche & Alexis Thomas** https://github.com/NathanDoumeche/Smart_mobility_challenge

## Authors
* **Aymeric Jan**
* **Paul Berhaut**
* **Yannig Goude** - (https://www.imo.universite-paris-saclay.fr/~goude/about.html)
* **Yvenn Amara-Ouali** - (https://www.yvenn-amara.com)

## License
* The dataset is licensed under the Open Database License (ODbL)
* The codes associated are licensed under the GNU GPLv3 License - see the [LICENSE.md](LICENSE.md) file for details
